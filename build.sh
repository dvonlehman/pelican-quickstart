# Create a python virtual env
# virtualenv venv

# Add venv/bin to the path
# export PATH="venv/bin:$PATH"

# Alternately you could include a requirements.txt in your repo and run
# pip install -r requirements.txt
pip install pelican markdown

# Run the pelican build
pelican content
